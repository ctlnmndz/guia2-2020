#!/usr/bin/env python
# -*- coding: utf-8 -*-

def leer_json():
    try:
        # Se lee el archivo
        with open('covid_19.csv', 'r') as archivo:
            lineas = archivo.readlines()
        archivo.close()

    except IOError:
        lineas = -1

    # Contenido del archivo
    return lineas


def procesar_datos(archivo):
    linea = None
    infectados = {}

    for linea in archivo:
        # Filtra las columnas separadas por coma
        aux = linea.split(',')
        # Columna de paises en el archivo original se guarda como diccionario
        paises_infectados = aux[3:4]
        casos_confirmados = aux[5:6]

        # Se evalua si los infectados es mayor a 1, si es así se añade el país
        # y la cantidad de infectados al diccionario  de infectados.

        #if (aux[5]>0 and infectados.get(aux[3])==None):
            #infectados[aux[3]] = aux[5]
        #else:
        #    valor = (aux[5]).replace(".0","")
        #    entValor = int(valor)
        #    tot = (infectados.__getitem__(aux[3])).replace(".0","")
        #    entTot = int(tot)
        #    infectados.__setitem__(aux[3],infectados.__getitem__(aux[3])+aux[5])

    # Se imprime los países infectados
    #print("Los países infectados son: "infectados.keys())
    # Se imprime la cantidad de países infectados
    #print("La cantidad de países infectados es de:", len(infectados))

# main
if __name__ == "__main__":
    archivo = leer_json()
    procesar_datos(archivo)
