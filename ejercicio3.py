#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json


# Función para eliminar claves y valores
def eliminar_aminoacido(diccionario_aminoacido):

    # Se ingresa el nombre de la clave a eliminar
    eliminar = input("Ingrese el aminoácido a eliminar:")
    # Se elimina la clave y a la vez también su valor
    del(diccionario_aminoacido[eliminar])
    print(diccionario_aminoacido)


# Función para editar la clave y el valor
def editar_aminoacido(diccionario_aminoacido):

    # Se pide los datos a editar
    editar = input("Ingrese el nombre aminoácido a editar :")
    nuevo = input("Ingrese el nuevo nombre del aminoácido: ")
    valor = input("ingrese la formula molecular del nuevo aminoácido: ")

    # Se edita el nombre de la clave
    diccionario_aminoacido[nuevo] = diccionario_aminoacido.pop(editar)
    # Se edita o se le asigana un nuevo valor a la clave
    diccionario_aminoacido[nuevo] = valor
    print(diccionario_aminoacido)


# Función para buscar valores de una clave
def buscar_aminoacido(diccionario_aminoacido):

    # Se pide el valor a buscar
    buscar = input("Ingrese la fórmula a buscar: ")
    valores = list(diccionario_aminoacido.values())
    #print(valores)

    # Se saca el indice de donde se encuentra el valor buscado
    #indice = valores.index(buscar)
    #print(indice)
    # Se imprime la clave (aminoácido) buscado
    #print(diccionario_aminoacido[])

# Función para ingresar claves y sus valores
def ingresar_aminoacido(diccionario_aminoacido):

    # Se pide el nombre de la clave (amonoácido) y su valor (fórmula molecular)
    aminoacido = str(input("Ingrese el nombre del aminoácido: "))
    formula = input("ingrese la fórmula molecular : ")

    # Se añade al diccionario
    diccionario_aminoacido[aminoacido] = formula
    print("\n")
    print("Aminoácidos: ", diccionario_aminoacido)


# Función principal
if __name__ == "__main__":

    diccionario_aminoacido = {}

    # Menú
    a = True
    while a:
        print("\n")
        print("ingrese:")
        print("1. Para ingresar un aminoácido al diccionario")
        print("2. Para buscar aminoácido")
        print("3. Para editar un aminoácido y su fórmula molecular")
        print("4. Para eliminar un aminoácido")
        print("5. Para salir")

        # Se llaman las funciones según lo pedido anteriormente
        marcador = int(input(""))
        if marcador == 1:
            ingresar_aminoacido(diccionario_aminoacido)
        elif marcador == 2:
            buscar_aminoacido(diccionario_aminoacido)
        elif marcador == 3:
            editar_aminoacido(diccionario_aminoacido)
        elif marcador == 4:
            eliminar_aminoacido(diccionario_aminoacido)
        else:
            a = False
