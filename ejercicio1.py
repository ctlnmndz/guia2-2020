#!/usr/bin/env python3
# -*- coding: utf-8 -*-


# Función para crear el nuevo diccionario
def nuevo_diccionario(valor, clave1, clave2, clave3, clave4):

    nuevo_dic = {}
    # Se le añade las nuevas claves y valores
    nuevo_dic[clave1] = valor
    nuevo_dic[clave2] = valor
    nuevo_dic[clave3] = valor
    nuevo_dic[clave4] = valor

    # Se imprime el nuevo diccionario
    print("El nuevo diccionario es:\n", nuevo_dic)


# Función para sacar el valor de las claves nuevas
def sacar_claves(dic, claves, valores):

    contador = 0
    valor = []
    clave1 = []
    clave2 = []
    clave3 = []
    clave4 = []

    # Ciclo para recorrer el valor del value
    for key, val in dic.items():
        for texto in val:
            contador = contador + 1
            # Va ir añadiendo el valor de las claves cada 2 letras del val
            if contador < 3:
                clave1.append(texto)
            elif contador > 2 and contador < 5:
                clave2.append(texto)
            elif contador > 4 and contador < 7:
                clave3.append(texto)
            else:
                clave4.append(texto)

    # A las claves de tamaño 2 se transforma en tamaño uno
    s = ""
    valor = s.join(claves)
    clave1 = s.join(clave1)
    clave2 = s.join(clave2)
    clave3 = s.join(clave3)
    clave4 = s.join(clave4)
    # Se llama la función para crear el nuevo diccionario
    nuevo_diccionario(valor, clave1, clave2, clave3, clave4)


# Función principal
if __name__ == "__main__":

    dic = {"Histidine" : "C6H9N3O2"}
    print("El diccionario original es:\n", dic)

    # Se transforma en una lista las keys y values
    claves = list(dic.keys())
    valores = list(dic.values())

    # Se llama la función para dar valores de las claves nuevas
    sacar_claves(dic, claves, valores)
